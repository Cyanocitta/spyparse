# A simple and flexible parser written in Python

The **PySparse** package lets you quickly parse files and strings. Be it a command to read, or a file, the aim of this parser is to give a way to write rules and apply them by chaining them together.

## The building blocks

At the core, there is the `Parser`, evaluating the given token tree given the message in question.

The `Selector` takes care of deciding which token to keep, based on their score.

The `Manager` is the one orchestrating the whole proscedure.

## Demonstration

The `test_demo.py` file is an example of how to quickly parse and identify some elements from a message. Those tokens are the one guiding the action once extracted.

### For example:

By giving to the parser this token tree:
```python
{ #is the command starting with '!', '?' or ';' ?
  "!?;":{ #then look for the next word
          self.parser.p_word:{ #if the word is one of them,
                               # return their token
                               "add":"Add",
                               "remove":"Remove",
                               "edit":"Edit"
                    },
          #if nothing else matches, it's at least a command
          self.parser.p_end: "Command"
        }
}

```

By inserting `!add`, the manager would run the actions corresponding to the extracted tokens:
```bash
❯ python test_demo.py
› initializing the manager
——— Sparse parsing library demo ———
your message: !add
» Add something.
execution ended.
```

The action takes place in `Manager.run`, once the extraction, `Manager.process` has ended. Here, we said to print the fact that a certain token has been extracted, and that, when there are any, there is at least one 'e' in the command.
```bash
❯ python test_demo.py
› initializing the manager
——— Sparse parsing library demo ———
your message: !remove
There's at least an 'e' in there ^^
» Remove something.
execution ended.
```
