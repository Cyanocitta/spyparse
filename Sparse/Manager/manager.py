from ..Parser import StrictParser
from ..Selector import ThresholdSelector

class Manager:
	parser = None
	selector = None
	
	command = None
	
	def __init__(self, parser=None, selector=None):
		if parser==None:
			self.parser = StrictParser()
		else:
			self.parser = parser
		if selector==None:
			self.selector = ThresholdSelector(self.parser)
		else:
			self.selector = selector#(self.parser)
		self.selector.maxFirst()
		self.selector.setThreshold(50)
	
	def process(self, message):
		return self._process(message)
	def run(self):
		return self._run()
		
	
	def execute(self, message):
		self.process(message)
		self.run()
	
	
	def _process(self, message):
		raise NotImplementedError("`Manager._process()` must be overridden!")
		##return NotImplemented
	def _run(self):
		raise NotImplementedError("`Manager._run()` must be overridden!")
		##return NotImplemented
