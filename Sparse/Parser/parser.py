
from ..Reader import Reader

import re

class Parser:
	
	reader = None#init
	token_chain = []#process
	
	token_tree_root = None
	
	pattern_type = "letter"# / word / regex
	
	
	def __init__(self):
		self.reader = Reader()
	#def __init__(self, reader):
	#	self.reader = reader
	
	def parse(self, message, token_tree_root):
		self.initTokenTree(token_tree_root)
		self.reader.setString(0, message)
		self.reader.select(0)
		self.token_chain += [self.__forward(self.token_tree_root)]
		return self
	
	def __applyPattern(self, string):
		return self._applyPattern(string)
	
	def __selectTarget(self, target=None):
		if target == None:
			self.reader.select(0)
		elif target != None:
			if not self.reader.select(target):
				pass#ERROR
	
	def __patternType(self, pType):
		self.pattern_type = pType
	
	def __forward(self, token_tree):
		end = None
		token = []
		pos = self.reader.position()
		for pattern in token_tree.keys():
			if pattern == self.p_end:
				end = pattern
				continue
			token += self.__parseToken(pattern, token_tree[pattern])
			self.reader.moveTo(pos)
		if end != None:
			token += self.__parseToken(end, token_tree[end])
			self.reader.moveTo(pos)
		return token
	
	def __call(self, token_tree, function, args=None):
		if args != None:
			function(token_tree, self.token_chain, *args)
		else:
			function(token_tree, self.token_chain)
	
	def __parseToken(self, pattern, token_tree=None):
		success = []
		if type(pattern) == tuple:
			if type(pattern[0]) == tuple:
				for t in pattern:
					#CALL
					success += [self.__call(token_tree, t[0], t[1:])]
			elif token_tree != None and type(pattern[0]) == str:
				for p in pattern:
					success += [self.__applyPattern(pattern)]
			elif token_tree == None and type(pattern[0]) == str:
				success = list(pattern)
			else:
				#CALL
				success = [self.__call(token_tree, pattern[0], pattern[1:])]
		elif callable(pattern):
			#CALL
			success = [self.__call(token_tree, pattern)]
		elif token_tree != None and type(pattern) == str:
			success = [self.__applyPattern(pattern)]
		elif token_tree == None and type(pattern) == str:
			success = [pattern]
		else:
			#ERR
			return None
		
		
		if token_tree != None:
			if type(token_tree) == dict:
				token_list = self.__forward(token_tree)
				for token_path in token_list:
					token_path[:0] = success
				return token_list
			else:
				return [success + self.__parseToken(token_tree)[0]]#token
		else:
			# success is the token
			return [success]
	
	
	# ####### Protected ####### #

	
	def _applyPattern(self, string):
		raise NotImplementedError("`Parser._applyPattern()` must be overridden!")
		##return NotImplemented
	
	
	
	# ####### Public ####### #
	
	def initTokenTree(self, token_tree):
		self.token_tree_root = token_tree
		self.pattern_type = "letter"
		self.reader.reset()
	
	def tokens(self):
		return self.token_chain
	
	
	
	
	
	# #### Patterns #### #            return a score tuple ->  (score, string segment)    i.e.:   (100, "set")
	def p_letter(self, token_tree, token_chain):
		self.__patternType("letter")
	def p_word(self, token_tree, token_chain):
		self.__patternType("word")
	def p_regex(self, token_tree, token_chain):
		self.__patternType("regex")
	
	def p_message(self, token_tree, token_chain):
		self.reader.select(0)
	
		##self.reader.select(0)
		pass
	
	def p_end(self, token_tree, token_chain):
		pass
	
	def t_dump(self, token_tree, token_chain):
		return (100, self.reader.read(-1))
	
	#       # #### Tokens #### #            return a token
	
