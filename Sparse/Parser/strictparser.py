from .parser import Parser

class StrictParser(Parser):
	
	def _applyPattern(self, string):
		init_pos = self.reader.position()
		if self.pattern_type == "letter":
			c = self.reader.read()
			if c != None:
				if c in string:
					return (100, c)
				else:
					self.reader.move(-1)
					return (0, "")
			else:
				return (0, "")
		elif self.pattern_type == "word":
			c = None
			s = ""
			for p in string:
				c = self.reader.read()
				if c != None:
					if c != p:
						self.reader.moveTo(init_pos)
						return (0, "")
				else:
					return (0, "")
				s += c
			return (100, s)
		elif self.pattern_type == "regex":
			matches = re.match(string, self.reader.read(-1), re.I)
			l = 0
			s = ""
			for m in matches:
				if len(m) > l:
					l = len(m)
					s = m
			if l > 0:
				self.reader.moveTo(init_pos+l)
				return (100, s)
			else:
				self.reader.moveTo(init_pos)
				return (0, "")
		else:
			#undefined
			return None
	
