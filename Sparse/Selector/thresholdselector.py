from .selector import Selector

class ThresholdSelector(Selector):
	
	def _filterTokenList(self, token_list_idx=-1):
		#raise NotImplementedError
		token_list = self.parser.tokens()[token_list_idx]
		
		token_path_idx = 0
		for token_path in token_list:
			cnt = 0
			score = 0
			for match_element in token_path:
				if type(match_element) == tuple and type(match_element[0]) in [int, float]:#, double]
					score += match_element[0]
					cnt += 1
			score /= cnt
			if self.comp(self.threshold, score) == self.threshold: #  if s <= t
				self.parser.tokens()[token_list_idx].pop(token_path_idx)
			token_path_idx += 1
	
	def _selectTokenList(self, token_list_idx=-1):
		#raise NotImplementedError
		token_paths = self.parser.tokens()[token_list_idx]
		
		if len(token_paths) < 1:
			return
		
		cnt = 0
		score = 0
		for match_element in token_paths[0]:
			if type(match_element) == tuple and type(match_element[0]) in [int, float]:#, double]
				score += match_element[0]
				cnt += 1
		score /= cnt
		best_score = score
		
		while len(token_paths) > 1:
			cnt = 0
			score = 0
			for match_element in token_paths[1]:
				if type(match_element) == tuple and type(match_element[0]) in [int, float]:#, double]
					score += match_element[0]
					cnt += 1
			score /= cnt
			if self.comp(score, best_score) != best_score:
				token_paths.pop(0)
				best_score = score
			else:
				token_paths.pop(1)
	
