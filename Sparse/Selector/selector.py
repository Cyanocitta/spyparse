
import re
from ..Parser import Parser

class Selector:
	
	parser = None#init
	
	threshold = None
	comp = max
	
	def __init__(self, parser, threshold=1):
		self.parser = parser
		self.threshold = threshold
		
	def filterTokenList(self, token_list_idx=-1):
		return self._filterTokenList(token_list_idx)
	def selectTokenList(self, token_list_idx=-1):
		return self._selectTokenList(token_list_idx)

	def _filterTokenList(self, token_list_idx=-1):
		raise NotImplementedError("`Selector._filterTokenList()` must be overridden!")
		##return NotImplemented
	def _selectTokenList(self, token_list_idx=-1):
		raise NotImplementedError("`Selector._selectTokenList()` must be overridden!")
		##return NotImplemented
		
	def setThreshold(self, threshold=None):
		if threshold == None:
			return self.threshold
		else:
			self.threshold = threshold
			return self.threshold
		
	def minFirst(self):
		self.comp = min
	def maxFirst(self):
		self.comp = max
