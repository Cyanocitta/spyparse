
class Reader:
	strings = {}#given
	strings_idx = {}#process
	current = None#given
	
	def __init__(self):
		pass
	
	def setString(self, name, string):
		self.strings[name] = string
		self.strings_idx[name] = 0
	
	def select(self, name):
		if name in self.strings.keys():
			self.current = name
			return True
		return False
	
	def moveTo(self, pos):
		if self.current == None:
			return False
		if pos == -1:
			if type(self.strings[self.current]) == list:
				pos = max([len(i)-1 for i in strings[self.current]])
			else:
				pos = len(strings[self.current])-1
		self.strings_idx[self.current] = pos
		return True
	
	def move(self, shift):
		if self.current == None:
			return False
		self.strings_idx[self.current] += shift
		return True
	
	def __read(self, target, length=1):
		if self.current == None:
			return None
		if self.strings_idx[self.current] < len(target):
			if length == -1:
				chars = target[self.strings_idx[self.current]:]
				self.strings_idx[self.current] = len(target)
				return chars
			else:
				chars = target[self.strings_idx[self.current]:self.strings_idx[self.current]+length]
				self.strings_idx[self.current] += length
				return chars
		else:
			return None
		
	def read(self, length=1):
		if self.current == None:
			return None
		if type(self.strings[self.current]) == list:
			chars_s = []
			for s in strings[self.current]:
				chars_s += [self.__read(length, s)]
			return chars_s
		else:
			return self.__read(self.strings[self.current], length)
	
	def delete(self, name):
		if self.current == None:
			return None
		if type(name) == str:
			try:
				return self.strings.pop(name)#idx
			except:
				return None
		elif type(name) == int and type(self.strings[self.current]) == list:
			return self.strings[self.current].pop(name)
	
	def currentString(self):
		if self.current == None:
			return None
		return self.strings[self.current]
	
	def position(self):
		if self.current == None:
			return None
		return self.strings_idx[self.current]
	
	def reset(self):
		self.strings = {}
		self.strings_idx = {}
		self.current = None
