from demomanager import DemoManager
#from ..Parser import Parser
#from ..Selector import Selector


if __name__ == "__main__":
	print("› initializing the manager")
	mgr = DemoManager()
	
	print("——— Sparse parsing library demo ———")
	
	msg = input("your message: ")
	
	mgr.execute(msg)
	
	print("execution ended.")
