from Sparse import Manager

class DemoManager(Manager):
	
	def _process(self, message):
		
		self.parser.parse(   message   ,
					{
						"!?;":{
								self.parser.p_word:{
														"add":"Add",
														"remove":"Remove",
														"edit":"Edit"
													},
								self.parser.p_end: "Command"
								}
					}
			)
		self.selector.filterTokenList()
		tks = self.parser.tokens()
		self.command = [token_list[0] if len(token_list)>0 else None for token_list in tks]
	
	def _run(self):
		#For example if the input is "!add", then self.command == [[(100, '!'), None, (100, 'add'), 'Add']]
		#                                                         first match  t_word   2nd match   end token
		if self.command == None:
			return
		if "Command" in self.command[0]:
			print("» It's some unmanaged command.")
		for element in self.command[0]:
			if type(element) == tuple and "e" in element[1]:
				print("There's at least an 'e' in there ^^")
		
		token = self.command[0][-1]
		if token == "Add":
			print("» Add something.")
		elif token == "Remove":
			print("» Remove something.")
		elif token == "Edit":
			print("» Edit something.")
		
